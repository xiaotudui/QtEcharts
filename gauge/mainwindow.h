#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace echarts {
class Gauge;
}

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    echarts::Gauge *webpage1;
    echarts::Gauge *webpage2;
    echarts::Gauge *webpage3;
    echarts::Gauge *webpage4;
};

#endif // MAINWINDOW_H
