#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QGridLayout>

#include <gauge.h>

using echarts::Gauge;

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);
    webpage1 = new Gauge;
    webpage2 = new Gauge;
    webpage3 = new Gauge;
    webpage4 = new Gauge;

    webpage1->show();
    webpage1->setTooltipFormatter("{a} <br/>当前温度 : {c}(℃)");
    webpage1->setName("装置温度");
    webpage1->setRange(QString::number(-20), QString::number(80));
    webpage1->setDataName("温度(℃)");
    webpage1->setDataValue(QString::number(24.5));

    auto layout = new QGridLayout;
    layout->addWidget(webpage1, 0, 0);
    layout->addWidget(webpage2, 0, 1);
    layout->addWidget(webpage3, 1, 0);
    layout->addWidget(webpage4, 1, 1);

    auto widget = new QWidget;
    widget->setLayout(layout);
    this->setCentralWidget(widget);
}

MainWindow::~MainWindow() { delete ui; }
