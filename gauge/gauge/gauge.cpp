#include "gauge.h"

#include <QWebEngineSettings>
#include <QMouseEvent>

namespace echarts {

Gauge::Gauge(QWidget *parent) : QWebEngineView(parent) {
    min_ = QString("0");
    max_ = QString("100");
    dataValue_ = QString("0");
    backGroundColor_ = QString("#1b1b1b");
    tooltipFormatter_ = QString("{a} <br/>{b} : {c}");
    axislColor_ = QString("[[0.2, 'lime'], [0.8, '#1e90ff'], [1, '#ff4500']]");
    createConnect();
    load(QUrl("file:///" + qApp->applicationDirPath() + "/html/gauge.html"));
    setContextMenuPolicy(Qt::NoContextMenu);
}

void Gauge::setBaceGroundColor(const QString &color) {
    backGroundColor_ = color;
    if (loadFinished_ && isShowed_) {
        QString js = QString("setBaceGroundColor('%1')").arg(color);
        page()->runJavaScript(js);
    } else {
    }
}

void Gauge::setTooltipFormatter(const QString &format) {
    tooltipFormatter_ = format;
    if (loadFinished_ && isShowed_) {
        QString js = QString("setTooltipFormatter(\"%1\")").arg(format);
        page()->runJavaScript(js);
    } else {
    }
}

void Gauge::setAxisColor(const QString &color) {
    axislColor_ = color;
    QString js = QString("setAxaisLineColor(%1)").arg(color);
    if (loadFinished_ && isShowed_) {
        page()->runJavaScript(js);
    } else {
    }
}

void Gauge::setName(const QString &name) {
    name_ = name;
    if (loadFinished_ && isShowed_) {
        QString js = QString("setGaugeName('%1')").arg(name);
        page()->runJavaScript(js);
    } else {
    }
}

void Gauge::setRange(const QString &min, const QString &max, int split) {
    min_ = min;
    max_ = max;
    split_ = split;
    if (loadFinished_ && isShowed_) {
        QString js = QString("setRange(%1, %2, %3)").arg(min).arg(max).arg(split);
        page()->runJavaScript(js);
    } else {
    }
}

void Gauge::setFontSize(int size) {
    fontSize_ = size;
    if (loadFinished_ && isShowed_) {
        QString js = QString("setFontSize(%1)").arg(size);
        page()->runJavaScript(js);
    } else {
    }
}

void Gauge::setDataName(const QString &name) {
    dataName_ = name;
    if (loadFinished_ && isShowed_) {
        QString js = QString("setDataName('%1')").arg(name);
        page()->runJavaScript(js);
    } else {
    }
}

void Gauge::setDataValue(const QString &value) {
    dataValue_ = value;
    if (loadFinished_ && isShowed_) {
        QString js = QString("setDataValue('%1')").arg(value);
        page()->runJavaScript(js);
    } else {
    }
}

void Gauge::hideEvent(QHideEvent *event) {
    isShowed_ = false;
    event->accept();
}

void Gauge::showEvent(QShowEvent *event) {
    isShowed_ = true;
    event->accept();
    initGauge();
}

void Gauge::initGauge() {
    setBaceGroundColor(backGroundColor_);
    setTooltipFormatter(tooltipFormatter_);
    setAxisColor(axislColor_);
    setName(name_);
    setRange(min_, max_, split_);
    setFontSize(fontSize_);
    setDataName(dataName_);
    setDataValue(dataValue_);
}

void Gauge::createConnect() {
    connect(this, &QWebEngineView::loadFinished, [&]() {
        this->loadFinished_ = true;
        this->initGauge();
    });
    connect(this, &QWebEngineView::loadStarted, [&]() { this->loadFinished_ = false; });
}

} // namespace echarts
