greaterThan(QT_MAJOR_VERSION, 4): QT += webenginewidgets
else: error("Qt版本太低，不支持")

SOURCES += \
    $$PWD/gauge.cpp

HEADERS += \
    $$PWD/gauge.h

INCLUDEPATH += \
    $$PWD

DISTFILES += \
    $$PWD/html/echarts.min.js \
    $$PWD/html/gauge.html
