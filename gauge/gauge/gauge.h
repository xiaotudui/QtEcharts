#ifndef GAUGE_H
#define GAUGE_H

#include <QObject>
#include <QtWebEngineWidgets>

namespace echarts {

class Gauge : public QWebEngineView {
    Q_OBJECT
public:
    explicit Gauge(QWidget *parent = nullptr);

public:
    /**
     * @brief setBaceGroundColor 设置背景颜色
     * @param color
     */
    void setBaceGroundColor(const QString &color);

    void setTooltipFormatter(const QString &format);

    void setAxisColor(const QString &color);
    /**
     * @brief setName 设置序列名称，用于tooltip的显示，legend 的图例筛选
     * @param name
     */
    void setName(const QString &name);

    /**
     * @brief setRange
     * @param min 最小的数据值，映射到 minAngle
     * @param max 最大的数据值，映射到 maxAngle
     * @param split 仪表盘刻度的分割段数。
     */
    void setRange(const QString &min, const QString &max, int split = 10);

    /**
     * @brief setFontSize
     * @param size 文字的字体大小
     */
    void setFontSize(int size);

    /**
     * @brief setDataName 设置数据项名称
     * @param name 数据项名称
     */
    void setDataName(const QString &name);

    /**
     * @brief setDataValue 设置数据值
     * @param value 数据值
     */
    void setDataValue(const QString &value);

signals:

public slots:

protected:
    virtual void hideEvent(QHideEvent *event) override;
    virtual void showEvent(QShowEvent *event) override;

private slots:
    void initGauge(void);

private:
    void createConnect(void);

private:
    QString name_;             /// 序列名称
    QString min_;              /// 最小的数据值
    QString max_;              /// 最大的数据值
    int split_ = 10;           /// 仪表盘刻度的分割段数
    int fontSize_ = 15;        /// 文字的字体大小
    QString dataName_;         /// 数据项名称
    QString dataValue_;        /// 数据值
    QString backGroundColor_;  /// 背景颜色  '#1b1b1b'
    QString tooltipFormatter_; /// 提示格式
    QString axislColor_;       /// 坐标轴颜色

    bool loadFinished_ = false;
    bool isShowed_ = false;
};

} // namespace echarts

#endif // GAUGE_H
